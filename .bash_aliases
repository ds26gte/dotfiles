# last change 2023-12-10

set -o vi

shopt -s cdspell
shopt -s extglob
shopt -s globstar
shopt -s nocaseglob
shopt -s autocd

bind 'set completion-ignore-case on'

test -f ~/dotfiles/aliases.sh && . ~/dotfiles/aliases.sh

if test ! -d /opt/cloudlinux; then
  fixcol
fi

test -f ~/dotfiles/env.sh && . ~/dotfiles/env.sh

if test \
  ! -d /gpfs -a \
  ! -d /sdcard -a  \
  ! -d /opt/cloudlinux -a \
  ! "$HOSTNAME" = penguin -a \
  ! "$VIM" -a \
  ! "$TMUX" ; then
  tmux new -s t$$
fi
