" last modified 2024-07-08 vi:ul=0xabc
au bufread */curriculum/{le,p}*/*.adoc setl tw=0
au bufread */{pyret-docs,typst}/*.html setl ft=lynxit
au bufread,bufnewfile *.t2p setf tex
au bufread,bufnewfile */curriculum/** setl mp=bpw
au bufread,bufnewfile */curriculum/*.asc setf asciidoc
au bufwritepre * if &ul == 0xabc | exec 'norm ms' | exec 'sort u' | exec 'norm `s' | endif
au filetype gnuplot,lss setl cms=#\ %s
au filetype lynxit setl ro | sil %!lynx -dump %
au filetype markdown,nroff,*tex,typst setl cpt+=k inf tw=65
au filetype nroff syn match endofbuffer /^\.PP/
au filetype typst nno <buffer> gO :ToC<cr><cr>
au filetype typst setl cms=//\ %s
au filetype typst setl fo+=n inde=
com! -nargs=0 -bar ZZcallvitinyyank sil! !vitinyyank
com! -nargs=0 -range Qyank <line1>,<line2>w! ~/tmp/.vitiny.clipboard.save | ZZcallvitinyyank | norm <c-l>
com! -nargs=0 Chartarr drop ~/src/code.pyret.org/src/web/arr/trove/chart.arr
com! -nargs=0 Chartlib drop ~/src/code.pyret.org/src/web/js/trove/chart-lib.js
com! -nargs=0 Cholecalciferol drop ~/dotfiles/plugin/cholecalciferol.vim
com! -nargs=0 Cyanocobalamin drop ~/dotfiles/plugin/cyanocobalamin.vim
com! -nargs=0 Gitdocs drop `ls -1 /usr/share/doc/git-doc/**/*.txt > ~/tmp/.filelist.gitdocs.save; echo ~/tmp/.filelist.gitdocs.save`
com! -nargs=0 Gitman drop `locate git-doc/user-manual.txt \| grep ^/usr`
com! -nargs=0 Jot e `echo ~/.notes/$(date +\%Y-\%m-\%d).adoc`
com! -nargs=0 Plus w | !plus %
com! -nargs=0 Pyreducer drop ~/src/pyret-lang/ide/src/reducer.ts
com! -nargs=0 Pyreg drop ~/src/pyret-lang/src/js/trove/multiple-regression.js
com! -nargs=0 Pystat drop ~/src/pyret-lang/src/arr/trove/statistics.arr
com! -nargs=0 Pytest drop ~/src/pyret-lang/tests/pyret/tests/test-statistics.arr
com! -nargs=0 Qpaste -r `vitinypaste; echo ~/tmp/.vitiny.clipboard.save` | redr
com! -nargs=0 Remixcss drop ~/src/curriculum/shared/langs/en-us/curriculum.css
com! -nargs=0 Remixmake drop ~/src/curriculum/lib/maker/Makefile.all
com! -nargs=0 Remixpreproc drop ~/src/curriculum/lib/preproc.rkt
com! -nargs=0 Remixreadme drop ~/src/curriculum/README.adoc
com! -nargs=0 Retinol drop ~/dotfiles/plugin/retinol.vim
com! -nargs=0 Sharedless drop ~/src/curriculum/lib/shared.less
com! -nargs=0 Todo drop ~/.notes/todo.adoc
com! -nargs=0 Vial drop ~/dotfiles/aliases.sh
com! -nargs=0 Vimrc drop ~/dotfiles/.vimrc
com! -nargs=0 Wpcsv drop ~/src/curriculum/lib/make-wp-csv.rkt
nm gx gy
