 " last modified 2024-10-03 vi:ul=0xabc
 if !has('nvim') | finish | endif
 let g:package_dir = split(&rtp, ',')[0] . '/pack/3rdparty/start/'
au termopen * tabe ~/.nvimRunningTerms | %d | 0s/^/1/ | noh | tabc | startins
com! -nargs=0 Timestamp 0s/^/last modified 1111-11-11\r/ | exec '1norm gcc' | w | e | exec 'norm yyp' | 2s/last modified/created/ | noh | w
com! -nargs=0 ToC sil exec 'lgrep ^= %' | lopen 6
com! -nargs=1 Cfile cf <args>
let &pa = '.,,' . glob('`git rev-parse --show-toplevel 2>/dev/null`') . '**'
set cul
set culopt=number
set dip+=algorithm:patience
set jop=view
set lcs+=leadmultispace:\ ,multispace:⏹
set scbk=100000
set sd+=%
tno <c-o> <c-\><c-n>
