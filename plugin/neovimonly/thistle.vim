 " last modified 2024-10-03 vi:ul=0xabc
 if !has('nvim') | finish | endif
if &uc >= 10000 | nno q :qa<cr>| set ls=1 ruf=%18(%#moremsg#--MORE--\ \ \ \ \ \ \ %P%) | endif
if isdirectory(g:package_dir . 'Colorizer') | exec 'au filetype css,less ColorHighlight' | endif
if isdirectory(g:package_dir . 'unicode.vim') | nno gA <Plug>(UnicodeGA)| endif
let g:nroff_is_groff = 1
let s:pyret_syn_file = ($PYRETHOME . '/tools/vim/syntax/pyret.vim') | if filereadable(s:pyret_syn_file) | exec 'au syntax pyret so ' . s:pyret_syn_file | endif
