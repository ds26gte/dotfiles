-- last modified 2024-07-03

package_dir = vim.fn.split(vim.o.rtp, ',')[1] .. '/pack/3rdparty/start/'

-- git signs

if vim.fn.isdirectory(package_dir .. 'vim-signify') ==0 and
  vim.fn.isdirectory(package_dir .. 'gitsigns.nvim') ~=0 then
  require('gitsigns').setup({
    signcolumn = false,
    numhl = true,
    on_attach = function(bufnr)
      local gs = package.loaded.gitsigns
      --
      vim.keymap.set('n', ']c', function()
        if vim.wo.diff then
          vim.cmd.normal({ ']c', bang = true })
        else
          gs.nav_hunk('next')
        end
      end, { buffer = bufnr })
      --
      vim.keymap.set('n', '[c', function()
        if vim.wo.diff then
          vim.cmd.normal({ '[c', bang = true })
        else
          gs.nav_hunk('prev')
        end
      end, { buffer = bufnr })
      --
      vim.keymap.set('n', '<leader>hp', gs.preview_hunk)
      vim.keymap.set('n', '<leader>hr', gs.reset_hunk)
      --
    end,
  })
  --
  vim.cmd('com! -nargs=0 Gblame Gitsigns blame')
  vim.cmd('com! -nargs=? Gdiff Gitsigns diffthis <q-args>')
end

-- surround

if vim.fn.isdirectory(package_dir .. 'nvim-surround') then
  require('nvim-surround').setup()
end
