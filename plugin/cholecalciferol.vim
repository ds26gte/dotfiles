" last modified 2024-10-03 vi:ul=0xabc
au bufnewfile COMMIT_EDITMSG startins
au bufread * sil! norm g`"
au bufread *.pdf setl ft= ro | sil %!pdftotext -nopgbrk % -
au bufread */.git/COMMIT_EDITMSG startins
au bufread ~/tmp/*.help.save setl ft=help
au bufread ~/tmp/.*.save setl ro
au bufread ~/tmp/.dict.*.help.save setl kp=:Dict
au bufread ~/tmp/.filelist.*.save nno <buffer> <cr> 0gf| nno <buffer> d :Explore <cfile><cr>
au bufread ~/tmp/.info.*.help.save setl kp=:Info
au bufread,bufnewfile *.scrbl setf racket
au bufread,bufnewfile *.txt setl ft=asciidoc
au bufread,bufnewfile *.{jpeg,JPG,jpg,PNG,png} ZZeogfile | bd
au bufwritepost * sil !vidatestamp %
au filetype * setl fo-=o fo-=r
au filetype asciidoc if &ro | nno <buffer> <space> <c-f>| nno <buffer> b <c-b>| endif
au filetype asciidoc nno <buffer> gO :ToC<cr>
au filetype asciidoc setl briopt= com=:// inf mp=adoc\ % tw=65
au filetype asciidoc setl flp+=\\\|^\\s*\\*\\+\\s\\+
au filetype asciidoc,gitcommit setl cpt+=k fo+=n
au filetype asciidoc,text,typescript setl cms=//\ %s
au filetype conf setl ft=sh
au filetype help nno <buffer> <space> <c-f>| nno <buffer> b <c-b>
au filetype help,man setl nu
au filetype make setl def=define
au filetype make setl isk+=-
au filetype make syn match redrawdebugrecompose /^ / containedin=ALL
au filetype netrw setl culopt=both nu
au filetype pyret setl cms=#\ %s
au filetype racket setl cms=;%s 
au filetype racket setl lw-=cond
au filetype typescript setl def=^\\s*\\(async\\s\\+\\)\\?\\(const\\\|function\\\|let\\\|var\\)
au quickfixcmdpost *cf*,*grep* cw 6
au syntax pyret setl isk-=:
au vimleave * sil! !vibackup
com! -narg=0 Jdir Explore ~/.notes
com! -nargs=* -complete=shellcmd Info drop `viinfo <args>`
com! -nargs=* Dict drop `export dictfile=~/tmp/.dict.$(PRNGen).help.save; dict <args> > $dictfile; echo $dictfile`
com! -nargs=* Ggrep Cfile `ggrep <args> > ~/tmp/.grep.qf.save; echo ~/tmp/.grep.qf.save`
com! -nargs=0 -bar ZZeogfile sil! !eog %
com! -nargs=0 Ftrim w | sil !ftrim %
com! -nargs=0 Rebash let s:bn = bufname('%') | bd! | exec "Bash" s:bn
com! -nargs=1 -complete=file Ddiff tabe `diff % <args> > ~/tmp/.diff.save; echo ~/tmp/.diff.save`
com! -nargs=? Bash tabe | exec "term" | keepalt file <args>
com! -nargs=? Lorem -r !lorem <args>
com! Gkommit sp | drop `gmsg`
ino <tab> <c-n>
ino jj <esc>
nno [L :lfirst\|echo <cr>
nno [Q :cfirst\|echo <cr>
nno [l :lprev\|echo<cr>
nno [q :cprev\|echo<cr>
nno ]L :llast\|echo <cr>
nno ]Q :clast\|echo <cr>
nno ]l :lnext\|echo<cr>
nno ]q :cnext\|echo<cr>
nno cd :Dict<space>
nno gy :sil !bb <cfile><cr>
set bri
set briopt=shift:4
set brk=\ "
set cb=unnamedplus
set cf
set cot+=longest
set def=^\\s*(def\\k*\\\|^\\s*fun\\%(ction\\)\\?\\\|^\\s*alias
set dict=/usr/share/dict/words
set et
set fcs=diff:␥,stl:~,stlnc:·
set flp=^\\s*-\\s\\+
set ic
set inc=^\\s*\\((require\\\|include::\\\|dofile\\s\\*(\\=\\)\\s*
set kp=:Dict
set lbr
set lcs+=trail:▮
set lcs-=trail:-
set mouse=
set noswf
set nows
set nu
set pm=.OG~
set sbr=⮩\ "
set scs
set spl=en_us
set sw=2
set wic
set wim=longest:full,full
set wiw=6
vno r "rd
