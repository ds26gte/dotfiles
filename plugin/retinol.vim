" last modified 2024-07-02 vi:ul=0xabc
hi changed guifg=cornflowerblue
hi comment guifg=darkseagreen
hi delimiter guifg=gray60
hi diffadd guifg=none
hi diffchange guifg=none
hi diffdelete none
hi difftext guifg=none
hi matchparen guibg=teal
hi modemsg none
hi nontext guifg=nvimdarkblue
hi normal guifg=none guibg=none
hi removed guifg=indianred
hi search guifg=none
hi specialkey guifg=skyblue
hi spellbad gui=none guibg=nvimdarkred
hi spellcap gui=none guibg=nvimdarkcyan
hi spelllocal gui=none guibg=nvimdarkgreen
hi spellrare gui=none guibg=nvimdarkblue
hi statement guifg=none
hi statuslinenc guifg=dimgray guibg=none
hi tablinesel gui=none guifg=nvimlightgray4
hi title guifg=none
hi! link added comment
hi! link cursorlinenr tablinesel
hi! link endofbuffer linenr
hi! link statusline statuslinenc
hi! link whitespace removed
