# last modified 2024-03-21
# Dorai Sitaram

. $HOME/.bashrc

if test -f $HOME/.cargo/env; then
  . "$HOME/.cargo/env"
fi

# opam configuration
test -r /home/ds26gte/.opam/opam-init/init.sh && . /home/ds26gte/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
