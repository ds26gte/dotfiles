-- last modified 2024-05-16

if os.getenv('MAKE_DIR') then
  return
end

function get_cmdline_settings()
  if #arg == 0 then return end
  local s = table.concat(arg, ' ')
  if not s:match('=') then return end
  load(s)()
end

function table.tostring(tbl)
  return '{' .. table.concat(tbl, ', ') .. '}'
end

get_cmdline_settings()
