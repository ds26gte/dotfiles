-- last modified 2024-07-10

function filled_p(b)
  -- every cell in the board is filled (non-false)
  return b[1][1] and
  b[1][2] and
  b[1][3] and
  b[2][1] and
  b[2][2] and
  b[2][3] and
  b[3][1] and
  b[3][2] and
  b[3][3]
end

function won_p(b)
  local b11, b12, b13 = b[1][1], b[1][2], b[1][3]
  -- row 1 is filled by one player
  if b11 and b11==b12 and b12==b13 then return true end
  local b21, b22, b23 = b[2][1], b[2][2], b[2][3]
  -- row 2
  if b21 and b21==b22 and b22==b23 then return true end
  local b31, b32, b33 = b[3][1], b[3][2], b[3][3]
  -- row 3
  if b31 and b31==b32 and b32==b33 then return true end
  -- col 1
  if b11 and b11==b21 and b21==b31 then return true end
  -- col 2
  if b12 and b12==b22 and b22==b32 then return true end
  -- col 3
  if b13 and b13==b23 and b23==b33 then return true end
  -- nw-se diag
  if b11 and b11==b22 and b22==b33 then return true end
  -- sw-ne diag
  if b31 and b31==b22 and b22==b13 then return true end
  --
  return false
end

function make_3x3_board()
  return {{nil, nil, nil}, {nil, nil, nil}, {nil, nil, nil}}
end

function file_exists_p(f)
  local h = io.open(f)
  -- open'ing a nonexistent file returns nil
  if h then h:close(); return true end
  return false
end

file_name_count = 0

function gen_file_name()
  local file_name
  repeat
    file_name_count = file_name_count + 1
    file_name = 'ttt' .. file_name_count .. '.txt'
  until not file_exists_p(file_name)
  return file_name
end

function play()
  local board = make_3x3_board()
  local file = gen_file_name()
  local outport = io.open(file, 'w')
  local player = 'O' -- first player is X!

  repeat
    player = (player == 'O') and 'X' or 'O'
    local tried = make_3x3_board()
    repeat
      local x, y = math.random(1, 3), math.random(1, 3)
      if not tried[x][y] then
        tried[x][y] = true
        if not board[x][y] then
          board[x][y] = player
          outport:write(player, x, y, '\n')
          break
        end
      end
    until filled_p(tried)
  until (filled_p(board) or won_p(board))

  if won_p(board) then
    outport:write(player .. ' won\n')
  else
    outport:write('draw\n')
  end
  outport:close()
end
