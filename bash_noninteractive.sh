# created < 2022-06
# last modified 2023-10-26
# Dorai Sitaram

if test -z "$VIM" -a -z "$MYVIMRC"; then
  return
  # do only for bash invoked from within vim
fi

if test -n "$MAKELEVEL"; then
  # shell calls from make within vim terminal should skip this
  return
fi

shopt -s expand_aliases

# :grep should recognize **

shopt -s globstar

. ~/dotfiles/aliases.sh
