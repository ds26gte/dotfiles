#! /usr/bin/env lua

-- created 2023-09-19
-- last modified 2023-09-19

local i = io.open('/usr/share/X11/rgb.txt')

local o = io.open('rgbh.txt', 'w')

for L in i:lines() do
  if L:match('^%s*%d+%s+%d+%s+%d+%s+.*') then
    local ni = L:gsub('^%s*(%d+).*', '%1')
    local nii = L:gsub('^%s*%d+%s+(%d+).*', '%1')
    local niii = L:gsub('^%s*%d+%s+%d+%s+(%d+).*', '%1')
    local hex = string.format('%02x%02x%02x', ni, nii, niii)
    L = hex .. '      ' .. L
  end
  o:write(L)
  o:write('\n')
end

o:close()
