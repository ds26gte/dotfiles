# last modified 2023-11-28
# created 2020-03-30
# Dorai Sitaram

adocf=$1

texf=${adocf%.*}.tex

asciidoctor-latex -a header=no $adocf

if test -n "$(kpsewhich texrc.sty)"; then
  texrc=texrc
else
  texrc=~/texrc
  test -f $texrc.sty || touch $texrc.sty
  texrc=$(echo $texrc | sed -e "s:^$HOME:\\\\string \~:")
fi

tmptexf=$(mktemp -p . tmpXXXXXXXXXX.tex)

echo \\documentclass{article} > $tmptexf
echo \\usepackage{$texrc} >> $tmptexf

title=$(grep '^= ' $adocf | head -n 1)

if test -n "$title"; then
  echo $title |
    sed -e 's/^= //' \
    -e "s/'/’/" \
    -e 's/.*/\\title{\0}/' >> $tmptexf
      echo \\date{} >> $tmptexf
fi

echo \\begin{document} >> $tmptexf

test -n "$title" && echo \\maketitle >> $tmptexf

# some fonts can't handle U+2009, introduced by asciidoctor-latex around
# en-dashes. Unfortunately, vi -es doesn't recognize this substitution, so
# doing it with sed

sed -i -e 's/ / /g' $texf

vi -es $texf <<EOF
%s/%/\\\\%/g

g/^\\\\\\]/+1 s/^$/\\\\vskip\\\\belowdisplayskip\\rZZ_blankline_after_display/
g/^ZZ_blankline_after_display/.,+1 join
%s/^ZZ_blankline_after_display \(\\\\hyp\)/\1/
%s/^ZZ_blankline_after_display/\\\\noindent/

w
EOF

cat $texf >> $tmptexf

echo \\end{document} >> $tmptexf

mv $tmptexf $texf
