" last modified 2024-07-09 vi:ul=0xabc
au bufread ~/tmp/.grep.qf.save nno <buffer> <cr> :ZZqfgotofile<cr>
au bufread,bufnewfile *.adoc setf asciidoc
au bufread,bufnewfile *.arr setf pyret
au bufread,bufnewfile *.rkt setf racket
au bufread,bufnewfile COMMIT_EDITMSG setf gitcommit
au bufread,bufnewfile [Mm]akefile* setf make
au filetype pyret setl isk+=-
au filetype racket setl lisp
com! -nargs=* Gdiff tabe `git diff <args> -- % > ~/tmp/.diff.save; echo ~/tmp/.diff.save`
com! -nargs=* Grep Cfile `fastgrep <args> > ~/tmp/.grep.qf.save; echo ~/tmp/.grep.qf.save`
com! -nargs=0 -range Gblame tabe `cd %:h; gblame %:p > ~/tmp/.gitblame.save; echo ~/tmp/.gitblame.save` | setl nowrap | :<line1> | norm zz
com! -nargs=0 Cwindow 6sp ~/tmp/.grep.qf.save | winc r | winc w
com! -nargs=0 Timestamp 0s/^/last modified 1111-11-11\r/ | noh | 1norm gcc
com! -nargs=0 ToC Grep '^=' %
com! -nargs=0 ZZqfgotofile sil on | 6sp | winc r | winc w | setl noro | mark z | %s/^\(.\{-}:\d\{-}\):🪣:/\1:/e | 'z | s/^\(.\{-}:\d\{-}\):/\1:🪣:/ | w | setl ro | norm 0gF
com! -nargs=1 Cfile sil e <args> | 0 | redr! | ZZqfgotofile
com! -nargs=? Explore e `export explorefile=~/tmp/.filelist.$(PRNGen).help.save; find -L <args> -maxdepth 1 \| sort \| sed -e 's/^\.\///' > $explorefile; echo $explorefile`
com! -range Vicomment <line1>,<line2> %!vicomment <line1> <line2> %:t
hi linenr ctermfg=240
hi statuslinenc cterm=none ctermfg=darkgray
hi tablinesel cterm=none ctermfg=247
hi visual ctermbg=240
hi! link tabline statuslinenc
hi! link tablinefill tabline
nno gcc :Vicomment<cr>
set ai
set ar
set bg=dark
set hf=$VIHELPFILE
set hid
set lcs-=eol:$ lcs+=tab:>\ ,nbsp:+
set ls=2
set ml
set nf-=octal
set nojs
set sm
set wmnu
sil ru! plugin/*.vim
sil ru! tinyvimonly/*.vim
sil! exec| if has('eval') | set hf& | endif
vm gc :Vicomment<cr>
